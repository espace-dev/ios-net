import os

from pysolardb.SolarDB import SolarDB
from tqdm import tqdm

from base import get_solar_radiation_data

output_dir = "/home/benjamin/Documents/PRO/PROJETS/BOULANGERIE_SOLAIRE/001_DATA/IN_SITU_DATA/SITES"
output_site_dir = "/home/benjamin/Documents/PRO/PROJETS/BOULANGERIE_SOLAIRE/001_DATA/IN_SITU_DATA"

solar = SolarDB()
solar.login(token="efe65d17f2cf5c7d6c983ebf650956b43cbf8426")

all_alias = [dic["alias"] for dic in solar.getCampaigns()]

for alias in tqdm(all_alias):

    df = get_solar_radiation_data(solar, alias, ["GHI", "DHI"], "avg")
    df.to_csv(os.path.join(output_dir, alias + ".csv"))
