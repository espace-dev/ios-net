import numpy as np
import pandas as pd
from pvlib.location import Location

from tqdm import tqdm


def check_string(string, list_of_strings):
    """ Check validity of and return string against list of valid strings

    Parameters
    ----------
    string: str
        searched string
    list_of_strings: list or tuple or set
        list of valid strings to be checked against

    Returns
    -------

    """
    output_string = []

    for item in list_of_strings:
        if item.lower().startswith(string.lower()):
            output_string.append(item)

    if len(output_string) == 1:
        return output_string[0]
    elif len(output_string) == 0:
        raise ValueError("input must match one of those: {}".format(list_of_strings))
    elif len(output_string) > 1:
        raise ValueError("input match more than one valid value among {}".format(list_of_strings))


def get_all_site_metadata(solar):
    """

    Parameters
    ----------
    solar

    Returns
    -------

    """

    metadata_df = None

    for dic in tqdm(solar.getCampaigns()):
        try:
            metadata_df = metadata_df.append(dic, ignore_index=True)
        except AttributeError:
            metadata_df = pd.DataFrame(columns=dic.keys())
            metadata_df = metadata_df.append(dic, ignore_index=True)

    return metadata_df


def get_dni(ghi, dhi, latitude, longitude, altitude):
    """ Get DNI time series from GHI and DHI time series

    Parameters
    ----------
    ghi: pandas.Series
        Pandas time series of ghi
    dhi: pandas.Series
        Pandas time series of dhi
    latitude: float
        location's latitude
    longitude: float
        location's longitude
    altitude: int or float
        location's altitude

    Returns
    -------

    """
    bhi = ghi - dhi
    location = Location(latitude, longitude, altitude=altitude)
    solar_position = location.get_solarposition(bhi.index)

    return bhi / solar_position["zenith"]


def get_solar_radiation_data(solar, alias, radiation_type, sensor_range):
    """ Get dataframe of solar components for specified location in IOS NET database

    Parameters
    ----------
    solar: pysolardb.SolarDB.SolarDB
        pysolardb SolarDB instance
    alias: str
        Alias of given station
    radiation_type: list
        list of valid solar radiation components
        "GHI", "DHI"
    sensor_range: str
        "avg", "max", "min"


    Returns
    -------

    """

    def choose_not_nan(x, y):
        return x if np.isnan(y) else y

    sensor_range = check_string(sensor_range, ["Avg", "Max", "Min"])
    series = []

    for rad in radiation_type:
        data = solar.getData(sites=[alias],
                             types=[rad],
                             start="1-1-2018")  # It seems starting date has no effect at all...

        for key in data[alias].keys():
            series.append(pd.Series(data=data[alias][key]["values"],
                                    index=pd.DatetimeIndex(data[alias][key]["dates"]),
                                    name=key))

    # Dataframe
    solar_radiation_df = pd.concat(series, axis=1)
    columns_to_drop = [col for col in solar_radiation_df.columns if sensor_range not in col]
    solar_radiation_df = solar_radiation_df.drop(columns=columns_to_drop)
    # df_with_no_nans = solar_radiation_df.fillna(value=0)

    for rad in radiation_type:
        rad_columns = [col for col in solar_radiation_df.columns if rad in col]
        # radiation = np.nanmean(solar_radiation_df[rad_columns], axis=1)
        # for rad_col in rad_columns[1::]:
        #     radiation += df_with_no_nans[rad_col]
        # radiation = radiation.combine(df[rad_col], choose_not_nan)

        solar_radiation_df[rad] = np.nanmean(solar_radiation_df[rad_columns], axis=1)

    return solar_radiation_df
